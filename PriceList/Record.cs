﻿namespace PriceList
{
    public class Record
    {
        public string Name { get; set; }
        public string Composition { get; set; }
        public string Weight { get; set; }
        public string Price { get; set; }
    }
}