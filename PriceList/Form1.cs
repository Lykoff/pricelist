﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PriceList
{
    public partial class Form1 : Form
    {
        public BindingList<Record> priceList;
        private System.Drawing.Printing.PrintDocument document;
        private int position = 0;
        private float dpi = 0;
        private int cardWidth = 120;
        private int cardHeight = 72;

        public Form1()
        {
            InitializeComponent();
            priceList = new BindingList<Record>();
            dataGridView1.DataSource = new BindingSource(priceList, null);
            priceList.Clear();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            priceList.Clear();
        }

        private void печатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            position = 0;
            if (priceList.Count == 0 || priceList[0].Name == null) return;

            document = new System.Drawing.Printing.PrintDocument();

            printPreviewDialog1.ClientSize = new Size(800, 600);
            printPreviewDialog1.Location = new System.Drawing.Point(29, 29);

            document.PrintPage +=
                    new System.Drawing.Printing.PrintPageEventHandler(document_PrintPage);

            document.PrinterSettings.DefaultPageSettings.Landscape = false;
            printPreviewDialog1.MinimumSize = new System.Drawing.Size(375, 250);
            printPreviewDialog1.Document = document;
            printPreviewDialog1.ShowDialog();

        }

        private void document_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            dpi = e.PageSettings.PaperSize.Height / 297.0f;

            Pen pen = new Pen(Color.Black, 1);

            e.Graphics.DrawLine(pen, new Point(0, 0), new Point(0, (int)(4 * cardHeight * dpi)));
            e.Graphics.DrawLine(pen, new Point(0, 0), new Point((int)((cardWidth + cardHeight) * dpi), 0));

            e.Graphics.DrawLine(pen, new Point((int)(cardWidth * dpi), 0), new Point((int)(cardWidth * dpi), (int)(4 * cardHeight * dpi)));
            e.Graphics.DrawLine(pen, new Point(0, (int)(cardHeight * dpi)), new Point((int)(cardWidth * dpi), (int)(cardHeight * dpi)));
            e.Graphics.DrawLine(pen, new Point(0, (int)(2 * cardHeight * dpi)), new Point((int)(120 * dpi), (int)(2 * cardHeight * dpi)));
            e.Graphics.DrawLine(pen, new Point(0, (int)(3 * cardHeight * dpi)), new Point((int)(120 * dpi), (int)(3 * cardHeight * dpi)));
            e.Graphics.DrawLine(pen, new Point(0, (int)(4 * cardHeight * dpi)), new Point((int)(120 * dpi), (int)(4 * cardHeight * dpi)));

            e.Graphics.DrawLine(pen, new Point((int)(cardWidth * dpi), (int)(cardWidth * dpi)), new Point((int)((cardWidth + cardHeight) * dpi), (int)(cardWidth * dpi)));
            e.Graphics.DrawLine(pen, new Point((int)(cardWidth * dpi), (int)(2 * cardWidth * dpi)), new Point((int)((cardWidth + cardHeight) * dpi), (int)(2 * cardWidth * dpi)));
            e.Graphics.DrawLine(pen, new Point((int)((cardWidth + cardHeight) * dpi), 0), new Point((int)((cardWidth + cardHeight) * dpi), (int)(2 * cardWidth * dpi)));

            int recsNum = priceList.Count - position;
            if (recsNum > 6) recsNum = 6;
            
            for (int i = 0; i < recsNum; i++)
            {
                switch (position % 6)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        drawHPrice(ref e);
                        break;
                    case 4:
                    case 5:
                        drawVPrice(ref e);
                        break;
                }
                position++;
            }

            e.HasMorePages = priceList.Count - position > 1 ? true : false;

            if (!e.HasMorePages) position = 0;
        }

        private void drawVPrice(ref PrintPageEventArgs e)
        {
            System.Drawing.Font printFontName = new System.Drawing.Font("Arial", 28, System.Drawing.FontStyle.Bold);
            System.Drawing.Font printFontSost = new System.Drawing.Font("Arial", 20, System.Drawing.FontStyle.Regular);
            System.Drawing.Font printFontPrice = new System.Drawing.Font("Arial", 44, System.Drawing.FontStyle.Bold);

            System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.NoClip;// | StringFormatFlags.DirectionRightToLeft;
            drawFormat.Alignment = StringAlignment.Center;

            System.Drawing.StringFormat drawFormatPrice = new System.Drawing.StringFormat();
            drawFormatPrice.FormatFlags = StringFormatFlags.NoClip;
            drawFormatPrice.Alignment = StringAlignment.Far;

            System.Drawing.StringFormat drawFormatAll = new System.Drawing.StringFormat();
            drawFormatAll.FormatFlags = StringFormatFlags.NoClip;
            drawFormatAll.Alignment = StringAlignment.Near;

            int numCard = position % 6;
            int offsetX = (cardWidth);
            int offsetY = (int)((numCard - 4) * cardWidth);

            RectangleF drawRectName = new RectangleF(dpi, (1) * dpi, (cardWidth - 2) * dpi, (cardHeight / 4) * dpi);
            RectangleF drawRectSost = new RectangleF(dpi, ((cardHeight / 3f) + 1) * dpi, (cardWidth - 2) * dpi, (1 * cardHeight / 2) * dpi);
            RectangleF drawRectWeight = new RectangleF(dpi, ((4 * cardHeight / 5) + 1) * dpi, (cardWidth / 3) * dpi, (cardHeight / 5) * dpi);
            RectangleF drawRectPrice = new RectangleF((cardWidth / 3) * dpi, ((0.72f * cardHeight) + 1) * dpi, (2 * cardWidth / 3) * dpi, (cardHeight / 5) * dpi);

            e.Graphics.TranslateTransform((cardHeight + cardWidth) * dpi, offsetY * dpi);
            e.Graphics.RotateTransform(90);
            e.Graphics.DrawString(priceList[position].Name, printFontName, System.Drawing.Brushes.Black, drawRectName, drawFormat);
            e.Graphics.DrawString(priceList[position].Composition, printFontSost, System.Drawing.Brushes.Black, drawRectSost);
            e.Graphics.DrawString(priceList[position].Weight, printFontName, System.Drawing.Brushes.Black, drawRectWeight);
            e.Graphics.DrawString(priceList[position].Price + "руб.", printFontPrice, System.Drawing.Brushes.Black, drawRectPrice, drawFormatPrice);
            e.Graphics.ResetTransform();
        }

        private void drawHPrice(ref PrintPageEventArgs e)
        {            
            System.Drawing.Font printFontName = new System.Drawing.Font("Arial", 28, System.Drawing.FontStyle.Bold);
            System.Drawing.Font printFontSost = new System.Drawing.Font("Arial", 20, System.Drawing.FontStyle.Regular);
            System.Drawing.Font printFontPrice = new System.Drawing.Font("Arial", 44, System.Drawing.FontStyle.Bold);

            int numCard = position % 6;
            int offset = (int)(numCard * cardHeight);

            System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.NoClip;
            drawFormat.Alignment = StringAlignment.Center;

            System.Drawing.StringFormat drawFormatPrice = new System.Drawing.StringFormat();
            drawFormatPrice.FormatFlags = StringFormatFlags.NoClip;
            drawFormatPrice.Alignment = StringAlignment.Far;

            RectangleF drawRectName = new RectangleF(dpi, (offset + 1) * dpi, (cardWidth - 2) * dpi, (cardHeight/4) * dpi);
            RectangleF drawRectSost = new RectangleF(dpi, (offset + (cardHeight / 3f) + 1) * dpi, (cardWidth - 2) * dpi, (1 * cardHeight / 2) * dpi);
            RectangleF drawRectWeight = new RectangleF(dpi, (offset + (4 * cardHeight / 5) + 1) * dpi, (cardWidth / 3) * dpi, (cardHeight / 5) * dpi);
            RectangleF drawRectPrice = new RectangleF((cardWidth / 3) * dpi, (offset + (0.72f * cardHeight) + 1) * dpi, (2 *cardWidth / 3) * dpi, (cardHeight / 5) * dpi);

            e.Graphics.DrawString(priceList[position].Name, printFontName, System.Drawing.Brushes.Black, drawRectName, drawFormat);
            e.Graphics.DrawString(priceList[position].Composition, printFontSost, System.Drawing.Brushes.Black, drawRectSost);
            e.Graphics.DrawString(priceList[position].Weight, printFontName, System.Drawing.Brushes.Black, drawRectWeight);
            e.Graphics.DrawString(priceList[position].Price + "руб.", printFontPrice, System.Drawing.Brushes.Black, drawRectPrice, drawFormatPrice);
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (priceList.Count == 0) return;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog1.FileName != "")
                {
                    var csv = new StringBuilder();

                    foreach(var onePrice in priceList)
                    {
                        if (onePrice.Name == null || onePrice.Name.Length == 0) 
                            continue;

                        var newLine = string.Format("{0}'{1}'{2}'{3}", onePrice.Name, onePrice.Composition, onePrice.Weight, onePrice.Price);
                        csv.AppendLine(newLine);
                    }

                    File.WriteAllText(saveFileDialog1.FileName, csv.ToString());
                }
            }
        }

        private void загрузитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                priceList.Clear();

                using (var reader = new StreamReader(openFileDialog1.FileName))
                {
                    List<string> listA = new List<string>();
                    List<string> listB = new List<string>();
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split('\'');

                        var onePrice = new Record { Name = values[0], Composition = values[1], Weight = values[2], Price = values[3]};
                        priceList.Add(onePrice);
                    }
                }
            }
        }
    }
}
